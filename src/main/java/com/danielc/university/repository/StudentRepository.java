package com.danielc.university.repository;

import com.danielc.university.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Daniel on 07/05/2017.
 */
public interface StudentRepository extends JpaRepository<Student, String> {

  Optional<Student> findByUserId(String userId);

  List<Student> findByEnrolledCoursesCode(String code);

}
