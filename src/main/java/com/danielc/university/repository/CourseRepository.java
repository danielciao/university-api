package com.danielc.university.repository;

import com.danielc.university.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by Daniel on 07/05/2017.
 */
public interface CourseRepository extends JpaRepository<Course, String> {

  @Query("SELECT c FROM Course c WHERE c.startDate >= :startDate AND c.endDate <= :endDate")
  List<Course> findByDate(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

  List<Course> findByStudentsUserId(String studentId);

}
