package com.danielc.university.repository;

import com.danielc.university.model.Lecturer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Daniel on 07/05/2017.
 */
public interface LecturerRepository extends JpaRepository<Lecturer, String> {
}
