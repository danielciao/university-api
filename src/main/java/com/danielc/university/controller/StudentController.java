package com.danielc.university.controller;

import com.danielc.university.model.Course;
import com.danielc.university.model.Student;
import com.danielc.university.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

import java.util.List;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

/**
 * Created by Daniel on 07/05/2017.
 */
@RestController
@RequestMapping("/student")
public class StudentController {

  private StudentService studentService;

  @Autowired
  public StudentController(StudentService studentService) {
    this.studentService = studentService;
  }

  @RequestMapping(
    value = "/{userId}",
    method = RequestMethod.GET,
    produces = {MediaType.APPLICATION_JSON_VALUE})
  public Student getStudent(@PathVariable String userId) {
    return studentService.getByUserId(userId);
  }

  @RequestMapping(
    method = RequestMethod.POST,
    produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<?> createStudent(@RequestBody Student student) {
    Student savedStudent = studentService.save(student);

    UriComponents uriComponents = MvcUriComponentsBuilder
      .fromMethodCall(on(StudentController.class).createStudent(student))
      .path("{userId}")
      .buildAndExpand(savedStudent.getUserId());

    return ResponseEntity.created(uriComponents.toUri()).build();
  }

  @RequestMapping(
    value = "/{userId}/courses",
    method = RequestMethod.GET,
    produces = {MediaType.APPLICATION_JSON_VALUE})
  public List<Course> getCourses(@PathVariable String userId) {
    return studentService.getEnrolledCoursesByUserId(userId);
  }

  @RequestMapping(
    value = "/{userId}",
    method = RequestMethod.DELETE)
  public void deleteStudent(@PathVariable String userId) {
    studentService.delete(userId);
  }

}
