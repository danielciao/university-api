package com.danielc.university.controller;

import com.danielc.university.model.Course;
import com.danielc.university.model.Student;
import com.danielc.university.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Daniel on 07/05/2017.
 */
@RestController
@RequestMapping("/course")
public class CourseController {

  private CourseService courseService;

  @Autowired
  public CourseController(CourseService courseService) {
    this.courseService = courseService;
  }

  @RequestMapping(
    method = RequestMethod.GET,
    produces = {MediaType.APPLICATION_JSON_VALUE})
  public List<Course> getAllCourses() {
    return courseService.getAll();
  }

  @RequestMapping(
    value = "/{code}/students",
    method = RequestMethod.GET,
    produces = {MediaType.APPLICATION_JSON_VALUE})
  public List<Student> getEnrolledStudentsForCourse(@PathVariable String code) {
    return courseService.getEnrolledStudentsForCode(code);
  }

  @RequestMapping(
    value = "/{code}",
    method = RequestMethod.DELETE,
    produces = {MediaType.APPLICATION_JSON_VALUE})
  public void delete(@PathVariable String code) {
    courseService.delete(code);
  }

}
