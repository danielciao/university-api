package com.danielc.university.service;

import com.danielc.university.model.Course;
import com.danielc.university.model.Student;
import com.danielc.university.repository.CourseRepository;
import com.danielc.university.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Created by Daniel on 07/05/2017.
 */
@Service
public class CourseService {

  private CourseRepository courseRepository;
  private StudentRepository studentRepository;

  @Autowired
  public CourseService(CourseRepository courseRepository, StudentRepository studentRepository) {
    this.courseRepository = courseRepository;
    this.studentRepository = studentRepository;
  }

  public List<Course> getAll() {
    return courseRepository.findAll();
  }

  public List<Student> getEnrolledStudentsForCode(String code) {
    return studentRepository.findByEnrolledCoursesCode(code)
      .stream()
      .map(student -> {
        student.setEnrolledCourses(null);
        return student;
      })
      .collect(toList());
  }

  public void delete(String courseCode) {
    courseRepository.delete(courseCode);
  }

}
