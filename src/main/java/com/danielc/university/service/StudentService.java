package com.danielc.university.service;

import com.danielc.university.exception.UserNotFoundException;
import com.danielc.university.model.Course;
import com.danielc.university.model.Student;
import com.danielc.university.repository.CourseRepository;
import com.danielc.university.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 07/05/2017.
 */
@Service
public class StudentService {

  private StudentRepository studentRepository;
  private CourseRepository courseRepository;

  @Autowired
  public StudentService(
    StudentRepository studentRepository,
    CourseRepository courseRepository) {
    this.studentRepository = studentRepository;
    this.courseRepository = courseRepository;
  }

  public Student getByUserId(String userId) {
    return studentRepository
      .findByUserId(userId)
      .orElseThrow(() -> new UserNotFoundException(userId));
  }

  public List<Course> getEnrolledCoursesByUserId(String userId) {
    if (getByUserId(userId) != null) {
      return courseRepository.findByStudentsUserId(userId);
    }
    return new ArrayList<>();
  }

  public void delete(String userId) {
    Student target = getByUserId(userId);
    if (target != null) {
      target.remove();
      studentRepository.delete(target);
    }
  }

  public Student save(Student student) {
    return studentRepository.save(student);
  }

}
