package com.danielc.university.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 07/05/2017.
 */
@Entity
public class Lecturer extends Person {

  @OneToMany(mappedBy = "lecturer")
  private List<Course> taughtCourses;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  @Column(columnDefinition = "DATE")
  private LocalDate joinedDate;

  public List<Course> getTaughtCourses() {
    return taughtCourses;
  }

  public void addTaughtCourse(Course taughtCourse) {
    if (this.taughtCourses == null) {
      this.taughtCourses = new ArrayList<>();
    }
    this.taughtCourses.add(taughtCourse);
  }

  public void setTaughtCourses(List<Course> taughtCourses) {
    this.taughtCourses = taughtCourses;
  }

  public LocalDate getJoinedDate() {
    return joinedDate;
  }

  public void setJoinedDate(LocalDate joinedDate) {
    this.joinedDate = joinedDate;
  }

}
