package com.danielc.university.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 07/05/2017.
 */
@Entity
public class Course {

  @Id
  @Column(unique = true, length = 7)
  private String code;
  private String title;
  private String description;

  @JsonIgnore
  @ManyToOne
  private Lecturer lecturer;

  @JsonIgnore
  @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinTable(
    name = "COURSE_STUDENT_RELATION",
    joinColumns = @JoinColumn(name = "COURSE_CODE", referencedColumnName = "CODE"),
    inverseJoinColumns = @JoinColumn(name = "STUDENT_ID", referencedColumnName = "USER_ID")
  )
  private List<Student> students;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  @Column(columnDefinition = "DATE")
  private LocalDate startDate;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  @Column(columnDefinition = "DATE")
  private LocalDate endDate;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Lecturer getLecturer() {
    return lecturer;
  }

  public void setLecturer(Lecturer lecturer) {
    this.lecturer = lecturer;
  }

  public List<Student> getStudents() {
    return students;
  }

  public void setStudents(List<Student> students) {
    this.students = students;
  }

  public void addStudent(Student student) {
    if (this.students == null) {
      this.students = new ArrayList<>();
    }
    this.students.add(student);
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

}
