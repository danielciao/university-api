package com.danielc.university.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by Daniel on 07/05/2017.
 */
@Entity
public class Student extends Person {

  @ManyToMany(mappedBy = "students", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  private List<Course> enrolledCourses;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  @Column(columnDefinition = "DATE")
  private LocalDate enrolledDate;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  @Column(columnDefinition = "DATE")
  private LocalDate graduateDate;

  public List<Course> getEnrolledCourses() {
    return enrolledCourses;
  }

  public void setEnrolledCourses(List<Course> enrolledCourses) {
    this.enrolledCourses = enrolledCourses;
  }

  public LocalDate getEnrolledDate() {
    return enrolledDate;
  }

  public void setEnrolledDate(LocalDate enrolledDate) {
    this.enrolledDate = enrolledDate;
  }

  public LocalDate getGraduateDate() {
    return graduateDate;
  }

  public void setGraduateDate(LocalDate graduateDate) {
    this.graduateDate = graduateDate;
  }

  public void remove() {
    enrolledCourses.forEach(course -> course.getStudents().remove(this));
    enrolledCourses = null;
  }

}
