package com.danielc.university;

import com.danielc.university.repository.CourseRepository;
import com.danielc.university.repository.LecturerRepository;
import com.danielc.university.repository.StudentRepository;
import com.danielc.university.util.DataLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
public class UniversityApplication {

  @Bean
  @Profile("!test")
  public DataLoader loadData(
    LecturerRepository lecturerRepository,
    StudentRepository studentRepository,
    CourseRepository courseRepository) {

    return new DataLoader(lecturerRepository, studentRepository, courseRepository);
  }

  public static void main(String[] args) {
    SpringApplication.run(UniversityApplication.class, args);
  }

}
