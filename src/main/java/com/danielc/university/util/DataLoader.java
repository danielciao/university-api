package com.danielc.university.util;

import com.danielc.university.model.Course;
import com.danielc.university.model.Lecturer;
import com.danielc.university.model.Student;
import com.danielc.university.repository.CourseRepository;
import com.danielc.university.repository.LecturerRepository;
import com.danielc.university.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.time.LocalDate;

/**
 * Created by Daniel on 07/05/2017.
 */
public class DataLoader {

  private LecturerRepository lecturerRepository;
  private StudentRepository studentRepository;
  private CourseRepository courseRepository;

  @Autowired
  public DataLoader(LecturerRepository lecturerRepository, StudentRepository studentRepository, CourseRepository courseRepository) {
    this.lecturerRepository = lecturerRepository;
    this.studentRepository = studentRepository;
    this.courseRepository = courseRepository;
  }

  @PostConstruct
  public void loadDate() {
    Lecturer lecturer = new Lecturer();
    lecturer.setUserId("danielc");
    lecturer.setName("Daniel");
    lecturer.setDateOfBirth(LocalDate.of(1984, 1, 25));
    lecturer.setJoinedDate(LocalDate.of(2010, 7, 1));

    Student student1 = new Student();
    student1.setUserId("jeremyl");
    student1.setName("Jeremy");
    student1.setDateOfBirth(LocalDate.of(1984, 1, 25));
    student1.setEnrolledDate(LocalDate.of(2016, 2, 10));

    Student student2 = new Student();
    student2.setUserId("amilyw");
    student2.setName("Amily");
    student2.setDateOfBirth(LocalDate.of(1984, 1, 25));
    student2.setEnrolledDate(LocalDate.of(2016, 2, 10));

    Student student3 = new Student();
    student3.setUserId("tonyb");
    student3.setName("Tony");
    student3.setDateOfBirth(LocalDate.of(1984, 1, 25));
    student3.setEnrolledDate(LocalDate.of(2016, 2, 10));

    Course course1 = new Course();
    course1.setCode("COMP101");
    course1.setTitle("Introduction to Computer Science");
    course1.setDescription("To introduce concepts and principles of problem solving by computer, and the construction of appropriate algorithms for the solution of problems.");
    course1.setLecturer(lecturer);
    course1.addStudent(student1);
    course1.addStudent(student3);
    course1.setStartDate(LocalDate.of(2017, 2, 25));
    course1.setEndDate(LocalDate.of(2017, 6, 30));

    Course course2 = new Course();
    course2.setCode("MATH101");
    course2.setTitle("Introduction to Calculus");
    course2.setDescription("A study of the fundamental techniques of calculus, including differentiation and integration for functions of one real variable, with applications to rate problems, graph sketching, areas and volumes.");
    course2.setLecturer(lecturer);
    course2.addStudent(student2);
    course2.addStudent(student3);
    course2.setStartDate(LocalDate.of(2017, 3, 1));
    course2.setEndDate(LocalDate.of(2017, 7, 5));

    lecturerRepository.save(lecturer);
    studentRepository.save(student1);
    studentRepository.save(student2);
    studentRepository.save(student3);
    courseRepository.save(course1);
    courseRepository.save(course2);
  }

}
