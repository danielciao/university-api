package com.danielc.university.repository;

import com.danielc.university.model.Course;
import com.danielc.university.model.Student;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by Daniel on 07/05/2017.
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
public class StudentRepositoryIT {

  @Autowired
  private StudentRepository candidate;

  @Autowired
  private TestEntityManager entityManager;

  @Before
  public void setup() {
    candidate.deleteAllInBatch();
  }

  @Test
  public void shouldFindStudentByUserId() {
    // Arrange
    Student newStudent = new Student();
    newStudent.setUserId("danielc");
    newStudent.setName("Daniel");
    newStudent.setDateOfBirth(LocalDate.of(1984, 1, 25));

    Student savedStudent = entityManager.persist(newStudent);

    // Act
    Student result = candidate.findByUserId(savedStudent.getUserId()).orElse(null);

    // Assert
    assertThat(result, is(notNullValue()));
    assertThat(result, allOf(
      hasProperty("userId", is("danielc")),
      hasProperty("name", is("Daniel")),
      hasProperty("dateOfBirth", is(LocalDate.of(1984, 1, 25)))
    ));
  }

  @Test
  public void shouldSaveStudent() {
    // Arrange
    Student newStudent = new Student();
    newStudent.setUserId("danielc");
    newStudent.setName("Daniel");
    newStudent.setDateOfBirth(LocalDate.of(1984, 1, 25));

    // Act
    String savedUserId = candidate.save(newStudent).getUserId();
    Student savedStudent = candidate.findByUserId(savedUserId).orElse(null);

    // Assert
    assertThat(savedStudent, is(notNullValue()));
    assertThat(savedStudent, allOf(
      hasProperty("userId", is("danielc")),
      hasProperty("name", is("Daniel")),
      hasProperty("dateOfBirth", is(LocalDate.of(1984, 1, 25)))
    ));
  }

  @Test
  public void shouldFindAllStudentsByCourseCode() {
    // Arrange
    Student student1 = new Student();
    student1.setUserId("danielc");
    student1.setName("Daniel");
    student1.setDateOfBirth(LocalDate.of(1984, 1, 25));

    Student student2 = new Student();
    student2.setUserId("tonyb");
    student2.setName("Tony");
    student2.setDateOfBirth(LocalDate.of(1979, 12, 25));

    Course course = new Course();
    course.setCode("COMP101");
    course.addStudent(student1);
    course.addStudent(student2);

    entityManager.persist(student1);
    entityManager.persist(student2);
    entityManager.persist(course);

    // Act
    List<Student> students = candidate.findByEnrolledCoursesCode(course.getCode());

    // Assert
    assertThat(students, hasSize(2));
  }

  @Test
  public void shouldDeleteStudent() {
    // Arrange
    Student newStudent = new Student();
    newStudent.setUserId("danielc");
    newStudent.setName("Daniel");
    newStudent.setDateOfBirth(LocalDate.of(1984, 1, 25));

    String savedUserId = entityManager.persist(newStudent).getUserId();

    // Act
    candidate.delete(savedUserId);
    Optional<Student> result = candidate.findByUserId(savedUserId);

    // Assert
    assertThat(result.isPresent(), is(false));
  }

}
