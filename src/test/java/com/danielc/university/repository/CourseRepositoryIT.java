package com.danielc.university.repository;

import com.danielc.university.model.Course;
import com.danielc.university.model.Student;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

/**
 * Created by Daniel on 07/05/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class CourseRepositoryIT {

  @Autowired
  private CourseRepository candidate;

  @Autowired
  private TestEntityManager entityManager;

  @Before
  public void setup() {
    candidate.deleteAllInBatch();
  }

  @Test
  public void shouldFindAllCoursesBetweenStartAndEndDate() {
    entityManager.persist(buildCourse("COMP101", LocalDate.of(2017, 3, 1), LocalDate.of(2017, 6, 30)));
    entityManager.persist(buildCourse("INFO101", LocalDate.of(2017, 4, 10), LocalDate.of(2017, 7, 1)));
    entityManager.persist(buildCourse("MATH101", LocalDate.of(2017, 2, 15), LocalDate.of(2017, 6, 10)));
    entityManager.persist(buildCourse("TECH101", LocalDate.of(2017, 2, 27), LocalDate.of(2017, 6, 20)));

    List<Course> courses = candidate.findByDate(LocalDate.of(2017, 2, 20), LocalDate.of(2017, 6, 30));

    assertThat(courses, hasSize(2));
  }

  @Test
  public void shouldFindAllCoursesByStudentId() {
    // Arrange
    Student newStudent = new Student();
    newStudent.setUserId("danielc");
    newStudent.setName("Daniel");
    newStudent.setDateOfBirth(LocalDate.of(1984, 1, 25));

    Course course1 = new Course();
    course1.setCode("COMP101");
    course1.addStudent(newStudent);

    Course course2 = new Course();
    course2.setCode("MATH101");
    course2.addStudent(newStudent);

    entityManager.persist(newStudent);
    entityManager.persist(course1);
    entityManager.persist(course2);

    // Act
    List<Course> courses = candidate.findByStudentsUserId(newStudent.getUserId());

    // Assert
    assertThat(courses, hasSize(2));
  }

  private Course buildCourse(String code, LocalDate startDate, LocalDate endDate) {
    Course course = new Course();
    course.setCode(code);
    course.setStartDate(startDate);
    course.setEndDate(endDate);

    return course;
  }

}
