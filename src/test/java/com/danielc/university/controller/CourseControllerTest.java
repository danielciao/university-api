package com.danielc.university.controller;

import com.danielc.university.model.Course;
import com.danielc.university.model.Student;
import com.danielc.university.service.CourseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Daniel on 07/05/2017.
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CourseController.class)
public class CourseControllerTest {

  private static final MediaType EXPECTED_JSON_CONTENT_TYPE = new MediaType(MediaType.APPLICATION_JSON, Charset.forName("UTF-8"));

  @MockBean
  private CourseService mockCourseService;

  @Autowired
  private CourseController candidate;

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void shouldReturnAllCourses() throws Exception {
    // Arrange
    List<Course> courses = new ArrayList<>();

    Course course1 = new Course();
    course1.setCode("COMP101");
    courses.add(course1);

    Course course2 = new Course();
    course2.setCode("MATH101");
    courses.add(course2);

    when(mockCourseService.getAll()).thenReturn(courses);

    // Act and Assert
    mockMvc.perform(get("/course").accept(MediaType.APPLICATION_JSON))
      .andExpect(content().contentType(EXPECTED_JSON_CONTENT_TYPE))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$", hasSize(2)))
      .andExpect(jsonPath("$[0].code", is("COMP101")))
      .andExpect(jsonPath("$[1].code", is("MATH101")));
  }

  @Test
  public void shouldDeleteCourse() throws Exception {
    mockMvc.perform(delete("/course/COMP101").accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(content().string(isEmptyString()));
  }

  @Test
  public void shouldGetEnrolledStudents() throws Exception {
    // Arrange
    List<Student> students = new ArrayList<>();

    Student student1 = new Student();
    student1.setUserId("danielc");
    students.add(student1);

    Student student2 = new Student();
    student2.setUserId("tonyb");
    students.add(student2);

    when(mockCourseService.getEnrolledStudentsForCode(eq("COMP101"))).thenReturn(students);

    // Act and Assert
    mockMvc.perform(get("/course/COMP101/students").accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$", hasSize(2)))
      .andExpect(jsonPath("$[0].userId", is("danielc")))
      .andExpect(jsonPath("$[1].userId", is("tonyb")));
  }

}
