package com.danielc.university.controller;

import com.danielc.university.exception.UserNotFoundException;
import com.danielc.university.model.Course;
import com.danielc.university.model.Student;
import com.danielc.university.service.StudentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Daniel on 07/05/2017.
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {StudentController.class})
public class StudentControllerTest {

  private static final MediaType EXPECTED_JSON_CONTENT_TYPE = new MediaType(MediaType.APPLICATION_JSON, Charset.forName("UTF-8"));

  @MockBean
  private StudentService mockStudentService;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void shouldReturn404IfStudentNotFound() throws Exception {
    when(mockStudentService.getByUserId(eq("nobody"))).thenThrow(new UserNotFoundException("nobody"));

    mockMvc.perform(get("/student/nobody")
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isNotFound());
  }

  @Test
  public void shouldFindStudentByUserId() throws Exception {
    Student student = new Student();
    student.setUserId("danielc");
    student.setName("Daniel");
    student.setDateOfBirth(LocalDate.of(1984, 1, 25));

    when(mockStudentService.getByUserId(eq("danielc"))).thenReturn(student);

    mockMvc.perform(get("/student/danielc")
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(content().contentType(EXPECTED_JSON_CONTENT_TYPE))
      .andExpect(jsonPath("$.userId", is("danielc")))
      .andExpect(jsonPath("$.name", is("Daniel")))
      .andExpect(jsonPath("$.dateOfBirth", is("1984-01-25")));
  }

  @Test
  public void shouldCreateStudent() throws Exception {
    Student newStudent = new Student();
    newStudent.setUserId("jonoc");
    newStudent.setName("Johnathan");
    newStudent.setDateOfBirth(LocalDate.of(1979, 12, 25));

    when(mockStudentService.save(any(Student.class))).thenReturn(newStudent);

    mockMvc.perform(post("/student")
      .accept(MediaType.APPLICATION_JSON)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(newStudent)))
      .andExpect(status().isCreated())
      .andExpect(header().string("location", is("http://localhost/student/jonoc")));
  }

  @Test
  public void shouldReturnAllCourses() throws Exception {
    Student student = new Student();
    student.setUserId("danielc");
    student.setName("Daniel");
    student.setDateOfBirth(LocalDate.of(1984, 1, 25));

    Course course1 = new Course();
    course1.setCode("COMP101");
    course1.addStudent(student);

    Course course2 = new Course();
    course2.setCode("MATH101");
    course2.addStudent(student);

    List<Course> enrolledCourses = new ArrayList<>();
    enrolledCourses.add(course1);
    enrolledCourses.add(course2);

    when(mockStudentService.getByUserId(eq("danielc"))).thenReturn(student);
    when(mockStudentService.getEnrolledCoursesByUserId(eq("danielc"))).thenReturn(enrolledCourses);

    mockMvc.perform(get("/student/danielc/courses")
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(content().contentType(EXPECTED_JSON_CONTENT_TYPE))
      .andExpect(jsonPath("$", hasSize(2)))
      .andExpect(jsonPath("$[0].code", is("COMP101")))
      .andExpect(jsonPath("$[1].code", is("MATH101")));
  }

  @Test
  public void shouldDeleteStudent() throws Exception {
    Student student = new Student();
    student.setUserId("danielc");
    student.setName("Daniel");
    student.setDateOfBirth(LocalDate.of(1984, 1, 25));

    when(mockStudentService.getByUserId(eq("danielc"))).thenReturn(student);

    mockMvc.perform(delete("/student/danielc")
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(content().string(isEmptyString()));
  }

}
